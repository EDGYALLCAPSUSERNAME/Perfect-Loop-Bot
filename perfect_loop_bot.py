import re
import time
import praw
import sqlite3
import OAuth2Util

# User config
# -----------------------------------------------------------------

# How long it waits (in hours) to check if comment
# has reached the minimum karma threshold
COMMENT_WAIT_TIME = 12

# The mininum karma threshold the comment must reach
# to be given user flair
MINIMUM_KARMA = 10

# Add any subreddits you wanted excluded to this list
EXCLUDED = ['perfectloops']

# The css class and text for the user flair
FLAIR_CSS_CLASS = ''
FLAIR_TEXT = ''

# The subject and body for the message to the user
USER_SUBJECT = 'You\'ve been given the ad flair in /r/perfectloops!'
USER_MESSAGE = """
You've just recieved the ad flair in /r/perfectloops for your
comment [here]({}). Thanks!
"""

# The subject and body for the message to the mod
MOD_SUBJECT = 'Someone just recieved ad flair'
MOD_MESAGE = """
/u/{} just recieved ad flair for [this comment]({}).
"""
# ------------------------------------------------------------------


def send_messages(r, username, link):
	print('Sending messages...')
	# First send a message to the user
	r.send_message(username, USER_SUBJECT, USER_MESSAGE.format(link),
		           captcha=None)

	# Then send a message to the mod
	r.send_messsage('djloreddit', MOD_SUBJECT,
			        MOD_MESAGE.format(username, link), captcha=None)


def flair_user(r, sql, username, link):
	print('Flairing user...')
	r.set_flair('perfectloops', username, flair_text=FLAIR_TEXT, 
			    flair_css_class=FLAIR_CSS_CLASS)
	send_messages(r, username, link)


def check_if_should_flair(r, sql, cur):
	for comment in cur.execute('SELECT * FROM comments'):
		# If it has been longer than the wait time
		if time.time() > float(comment[1]) + (COMMENT_WAIT_TIME * 3600):
			flair_user(r, sql, comment[2], comment[3])
			cur.execute('DELETE FROM comments WHERE ID=?', [comment[0]])
			sql.commit()


def find_comments(r, sql, cur):
	i = 0  # comment counter
	print('Browsing comments...')
	comment_stream = praw.helpers.comment_stream(r, 'all')
	for comment in comment_stream:
		# Skip if in excluded
		if comment.subreddit.display_name in EXCLUDED:
			continue

		# If it says /r/perfectloops add it to the database
		if re.search('/r/perfectloops', str(comment), re.IGNORECASE):
			print('Found a comment...')
			cur.execute('INSERT INTO comments VALUES(?, ?, ?, ?)', [str(comment.id), 
													                str(comment.created),
													                str(comment.author),
													                str(comment.permalink)])
			sql.commit()

		# Check if it needs to flair users every 100 comments
		if not i % 1000:
			print('Checking if I should flair any...')
			check_if_should_flair(r, sql, cur)

		i += 1


def main():
	r = praw.Reddit(user_agent='PerfectLoopFlair v1.0 /u/EDGYALLCAPSUSERNAME')
	o = OAuth2Util.OAuth2Util(r, print_log=True)

	# Setup/Load the database
	sql = sqlite3.connect('plbot.db')
	cur = sql.cursor()
	cur.execute('CREATE TABLE IF NOT EXISTS comments(ID TEXT, CREATED TEXT, USER TEXT, LINK TEXT)')
	print('Loaded SQL Database')
	sql.commit()

	while True:
		try:
			o.refresh()
			find_comments(r, sql, cur)
		except Exception as e:
			print('ERROR: {}'.format(e))
			time.sleep(500)

if __name__ == '__main__':
	main()
